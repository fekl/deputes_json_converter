package org.fekl.tool

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.File
import java.io.PrintStream


val PHONE_NUMBER_REGEX = """Téléphone : (\d{2}(?: |\.)\d{2}(?: |\.)\d{2}(?: |\.)\d{2}(?: |\.)\d{2})""".toRegex()


/** The data model of NosDeputes.fr website. */
class NosDeputesFr(val deputes: List<Deputes> = emptyList()) {
    data class Collaborateur(val collaborateur: String = "")

    data class Mandat(val mandat: String = "")

    data class SitesWeb(val site: String = "")

    data class Emails(val email: String = "")

    data class Adresses(val adresse: String = "")

    data class Depute(val num_deptmt: String = "",
                      val sexe: Char,
                      val url_nosdeputes: String = "",
                      val nom: String = "",
                      val emails: List<Emails>,
                      val nb_mandats: Int = 0,
                      val twitter: String = "",
                      val nom_de_famille: String = "",
                      val place_en_hemicycle: String = "",
                      val nom_circo: String = "",
                      val anciens_mandats: List<Mandat>,
                      val autres_mandats: List<Mandat>,
                      val anciens_autres_mandats: List<Mandat>,
                      val mandat_debut: String = "",
                      val id: Int = 0,
                      val prenom: String = "",
                      val collaborateurs: List<Collaborateur>?,
                      val url_an: String = "",
                      val date_naissance: String = "",
                      val slug: String = "",
                      val lieu_naissance: String? = "",
                      val profession: String? = "",
                      val parti_ratt_financier: String = "",
                      val num_circo: Int = 0,
                      val sites_web: List<SitesWeb>,
                      val id_an: String = "",
                      val adresses: List<Adresses>,
                      val groupe_sigle: String = "",
                      val url_nosdeputes_api: String = "")

    data class Deputes(val depute: Depute)
}

/** The data model of Fekl.org deputes. */
class FeklDepute(val firstName: String,
                 val lastName: String,
                 val gender: Gender,
                 val departmentCode: String,
                 val electoralAreaCode: Int,
                 val addresses: List<Address>,
                 val emails: List<String>) {

    enum class Gender { MALE, FEMALE }

    class Address(val postalAddress: String, val phoneNumbers: List<String>)
}

/**
 * Converts a JSON file frome NosDeputes.fr to a JSON file usable by Fekl.org.
 *
 * The file to convert path is given as first element of [args]. Error messages
 * are print in [errorOutput] when [args] is empty, or does not have a valide
 * file path as first element, or if the path points to file that cannot be
 * parsed as a NosDeputes.fr JSON.
 *
 * The result is print in [standardOutput].
 */
class ConverterCli(private val standardOutput: PrintStream, private val errorOutput: PrintStream) {

    fun convert(args: Array<String>) {
        if (args.isEmpty()) {
            this.errorOutput.println("Please, provide a JSON file path as argument.")
            return;
        }

        val inputFile = File(args.first())
        if (!inputFile.exists()) {
            this.errorOutput.println("The file path provided as argument does not exist.")
            return
        }

        val mapper = jacksonObjectMapper()
        val nosDeputesJson = try {
            mapper.readValue<NosDeputesFr>(inputFile)
        } catch (e: Exception) {
            this.errorOutput.println("The given file is not a valid JSON from NosDeputes.fr.")
            return
        }

        val list = nosDeputesJson.deputes.map {
            FeklDepute(it.depute.prenom,
                    it.depute.nom,
                    mapSex(it.depute.sexe),
                    it.depute.num_deptmt,
                    it.depute.num_circo,
                    it.depute.adresses.map { mapAddresse(it.adresse) },
                    it.depute.emails.map { it.email })
        }
        val writer = mapper.writerWithDefaultPrettyPrinter()
        this.standardOutput.println(writer.writeValueAsString(list))
    }

    private fun mapSex(sex: Char) = when (sex) {
        'H' -> FeklDepute.Gender.MALE
        'F' -> FeklDepute.Gender.FEMALE
        else -> throw IllegalArgumentException("Unknown sex value.")
    }

    private fun mapAddresse(adresse: String): FeklDepute.Address {
        val matchResults = PHONE_NUMBER_REGEX.findAll(adresse).toList()

        return if (matchResults.isEmpty()) {
            FeklDepute.Address(adresse, emptyList())
        } else {
            FeklDepute.Address(adresse.substring(0, matchResults[0].range.start - 1), matchResults.map { it.groupValues[1].replace('.', ' ') })
        }
    }
}

/**
 * Main function.
 *
 * Takes a JSON file path as argument and prints the result. It olny configures
 * converter command line interface to redirect messages to standard outputs.
 */
fun main(args: Array<String>) {
	ConverterCli(System.out, System.err).convert(args)
}
