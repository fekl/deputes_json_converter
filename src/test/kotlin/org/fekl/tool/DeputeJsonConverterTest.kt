package org.fekl.tool

import net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.io.ByteArrayOutputStream
import java.io.PrintStream

const val TEST_FILE = "/nosdeputes.fr_deputes_en_mandat_2018-09-25.json"
const val TEST_RESULT_FILE = "/fekl_deputes_20180925.json"
const val TEST_CORRUPTED_FILE = "/nosdeputes.fr_corrupted.json"

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DeputeJsonConverterTest {
	
	@Test
	fun converterCliWithEmptyArgs() {
		val standardOutput = ByteArrayOutputStream()
		val errorOutput = ByteArrayOutputStream()
		
		ConverterCli(PrintStream(standardOutput), PrintStream(errorOutput)).convert(arrayOf<String>())
		
		assertEquals(0, standardOutput.size())
		assertEquals("Please, provide a JSON file path as argument.\n", errorOutput.toString())
	}
	
	@Test
	fun converterCliWithUnknownFilePath() {
		val standardOutput = ByteArrayOutputStream()
		val errorOutput = ByteArrayOutputStream()
		
		ConverterCli(PrintStream(standardOutput), PrintStream(errorOutput)).convert(arrayOf<String>("/nofile.json"))
		
		assertEquals(0, standardOutput.size())
		assertEquals("The file path provided as argument does not exist.\n", errorOutput.toString())
	}
	
	@Test
	fun mainWithValidJson() {
		val standardOutput = ByteArrayOutputStream()
		val errorOutput = ByteArrayOutputStream()
		
		ConverterCli(PrintStream(standardOutput), PrintStream(errorOutput)).convert(arrayOf(getResourcePath(TEST_FILE)))

		assertEquals(0, errorOutput.size())
		assertJsonEquals(getResourceContent(TEST_RESULT_FILE), standardOutput.toString())
	}
	
	@Test
	fun mainWithInvalidValidJson() {
		val standardOutput = ByteArrayOutputStream()
		val errorOutput = ByteArrayOutputStream()
		
		ConverterCli(PrintStream(standardOutput), PrintStream(errorOutput)).convert(arrayOf(getResourcePath(TEST_CORRUPTED_FILE)))
		
		assertEquals(0, standardOutput.size())
		assertEquals("The given file is not a valid JSON from NosDeputes.fr.\n", errorOutput.toString())
	}
	
	private fun getResourcePath(resourceName: String): String {
		return DeputeJsonConverterTest::class.java.getResource(resourceName).toURI().path
	}
	
	private fun getResourceContent(resourceName: String): String {
		return DeputeJsonConverterTest::class.java.getResource(resourceName).readText();
	}
}