# DeputesJsonConverter

Converts a JSON file from [NosDéputés.fr](https://www.nosdeputes.fr/) to a Fekl's deputes JSON model.

# Build

The project can be built using Maven. All you have to do is running

```console
mvn package
```

It will produce a jar named `deputes_json_converter-X.Y.Z.jar` where `X.Y.Z` is the version (following [Semantic Vesioning](https://semver.org/)).

# Usage

You need to use at least JRE 8 to run the tool.

```console
java -jar deputes_json_converter-X.Y.Z.jar path/to/json_file.json
```
